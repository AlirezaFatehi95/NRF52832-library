#ifndef __IR_NRF52832_GPIO_H__
#define __IR_NRF52832_GPIO_H__

#include "IR_NRF52832_Common.h"

typedef enum
{
  IR_NRF_GPIO_NO_PULL   = 0,
  IR_NRF_GPIO_PULL_DOWN = 1,
  IR_NRF_GPIO_PULL_UP   = 3
}IR_NRF_GPIO_PullTypeDef;

//Standard Power (Vdd > 1.7, 0.5mA)
//High Power (Vdd > 2.7, 5mA)
//0 -> output low state (current sink)
//1 -> output high state (current source)
typedef enum
{
  IR_NRF_GPIO_DRIVE_STANDARD_0_STANDARD_1   = 0,
  IR_NRF_GPIO_DRIVE_HIGH_0_STANDARD_1       = 1,
  IR_NRF_GPIO_DRIVE_STANDARD_0_HIGH_1       = 2,
  IR_NRF_GPIO_DRIVE_HIGH_0_HIGH_1           = 3,
  IR_NRF_GPIO_DRIVE_DISCONNECT_0_STANDARD_1 = 4,
  IR_NRF_GPIO_DRIVE_DISCONNECT_0_HIGH_1     = 5,
  IR_NRF_GPIO_DRIVE_STANDARD_0_DISCONNECT_1 = 6,
  IR_NRF_GPIO_DRIVE_HIGH_0_DISCONNECT_1     = 7
}IR_NRF_GPIO_DriveTypeDef;

typedef enum
{
  IR_NRF_GPIO_SENSE_DISABLE = 0,
  IR_NRF_GPIO_SENSE_HIGH    = 2,
  IR_NRF_GPIO_SENSE_LOW     = 3
}IR_NRF_GPIO_SenseTypeDef;

typedef enum
{
  IR_NRF_GPIO_OUTPUT_RESET  = 0,
  IR_NRF_GPIO_OUTPUT_SET    = 1,
	IR_NRF_GPIO_OUTPUT_TOGGLE = 2
}IR_NRF_GPIO_OutputTypeDef;

typedef enum
{
  IR_NRF_GPIO_MODE_DISABLE = 0,
  IR_NRF_GPIO_MODE_EVENT   = 1,
  IR_NRF_GPIO_MODE_TASK    = 3
}IR_NRF_GPIO_ModeTypeDef;

typedef enum
{
  IR_NRF_GPIO_TRANSITION_None      = 0,
  IR_NRF_GPIO_TRANSITION_LOWTOHiGH = 1,
  IR_NRF_GPIO_TRANSITION_HIGHTOLOW = 2,
  IR_NRF_GPIO_TRANSITION_TOGGLE    = 3
}IR_NRF_GPIO_PlarityTypeDef;

typedef enum
{
  IR_NRF_GPIO_INTERRUPT_CHANNEL0 = 0,
  IR_NRF_GPIO_INTERRUPT_CHANNEL1 = 1,
  IR_NRF_GPIO_INTERRUPT_CHANNEL2 = 2,
  IR_NRF_GPIO_INTERRUPT_CHANNEL3 = 3,
  IR_NRF_GPIO_INTERRUPT_CHANNEL4 = 4,
  IR_NRF_GPIO_INTERRUPT_CHANNEL5 = 5,
  IR_NRF_GPIO_INTERRUPT_CHANNEL6 = 6,
  IR_NRF_GPIO_INTERRUPT_CHANNEL7 = 7
}IR_NRF_GPIO_Interrupt_StreamTypeDef;

void IR_NRF_GPIO_Init(bool latch_en, bool irq_en, uint32_t irq_priority);
void IR_NRF_GPIO_Deinit(void);
void IR_NRF_GPIO_Input_Pin_Init(uint8_t pin, IR_NRF_GPIO_PullTypeDef pull, IR_NRF_GPIO_SenseTypeDef sense);
void IR_NRF_GPIO_Output_Pin_Init(uint8_t pin, IR_NRF_GPIO_DriveTypeDef drive);
void IR_NRF_GPIO_Write_Pin(uint8_t pin, IR_NRF_GPIO_OutputTypeDef output);
bool IR_NRF_GPIO_Read_Pin(uint8_t pin);
bool IR_NRF_GPIO_Read_Latch(uint8_t pin);
void IR_NRF_GPIO_PortEvent_Enable(void);
void IR_NRF_GPIO_PortEvent_Disable(void);
void IR_NRF_GPIO_EXTI_Pin_Init(uint8_t pin, IR_NRF_GPIO_ModeTypeDef mode, IR_NRF_GPIO_PlarityTypeDef polarity,
                               IR_NRF_GPIO_OutputTypeDef out_init, IR_NRF_GPIO_Interrupt_StreamTypeDef stream);
void IR_NRF_GPIO_EXTI_Pin_Deinit(IR_NRF_GPIO_Interrupt_StreamTypeDef stream);
void IR_GPIOE_Callback(IR_NRF_GPIO_Interrupt_StreamTypeDef stream);
#endif /*__IR_NRF52832_GPIO_H__*/
