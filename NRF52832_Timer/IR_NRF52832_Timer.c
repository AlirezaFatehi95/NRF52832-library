#include "IR_NRF52832_Timer.h"

//if(irq_en == true) then: irq_priority must be between 0 to 7
void IR_NRF_Timer_Init(NRF_TIMER_Type* timer, IR_NRF_Timer_ModeTypeDef mode, IR_NRF_Timer_BitmodeTypeDef bitband,
                          IR_NRF_Timer_PrescalerTypeDef prescaler, bool irq_en, uint32_t irq_priority)
{
  timer->TASKS_STOP = 1;
  timer->TASKS_CLEAR = 1;
  timer->MODE = mode;
  timer->BITMODE = bitband;
  timer->PRESCALER = prescaler;
  if(irq_en && irq_priority < 8)
  {
		if(timer == NRF_TIMER0)
		{
			NVIC_SetPriority(TIMER0_IRQn, irq_priority);
			NVIC_EnableIRQ(TIMER0_IRQn);
		}
		else if(timer == NRF_TIMER1)
		{
			NVIC_SetPriority(TIMER1_IRQn, irq_priority);
			NVIC_EnableIRQ(TIMER1_IRQn);
		}
		else if(timer == NRF_TIMER2)
		{
			NVIC_SetPriority(TIMER2_IRQn, irq_priority);
			NVIC_EnableIRQ(TIMER2_IRQn);
		}
		else if(timer == NRF_TIMER3)
		{
			NVIC_SetPriority(TIMER3_IRQn, irq_priority);
			NVIC_EnableIRQ(TIMER3_IRQn);
		}
		else
		{
			NVIC_SetPriority(TIMER4_IRQn, irq_priority);
			NVIC_EnableIRQ(TIMER4_IRQn);
		}
  }
}

void IR_NRF_Timer_Deinit(NRF_TIMER_Type* timer)
{
  timer->TASKS_STOP = 1;
  timer->TASKS_CLEAR = 1;
	if(timer == NRF_TIMER0)
	{
		NVIC_DisableIRQ(TIMER0_IRQn);
	}
	else if(timer == NRF_TIMER1)
	{
		NVIC_DisableIRQ(TIMER1_IRQn);
	}
	else if(timer == NRF_TIMER2)
	{
		NVIC_DisableIRQ(TIMER2_IRQn);
	}
	else if(timer == NRF_TIMER3)
	{
		NVIC_DisableIRQ(TIMER3_IRQn);
	}
	else
	{
		NVIC_DisableIRQ(TIMER4_IRQn);
	}
}

//note that TIMER0, TIMER1 and TIMER2 have 4 channels instead of 6
//only the shortest repetetive channel my take affect
void IR_NRF_Timer_Channel_Init(NRF_TIMER_Type* timer, IR_NRF_Timer_ChannelTypeDef channel,
                              uint32_t capture_compare, bool event_en, bool repetetive_en)
{
  timer->CC[channel] = capture_compare;
  if(event_en)
	{
		if(repetetive_en)
		{
			timer->SHORTS |= 1 << channel;
		}
		else
		{
			timer->SHORTS &= ~(1 << channel);
		}
		timer->INTENSET = 1 << (16 + channel);
	}
}

//note that TIMER0, TIMER1 and TIMER2 have 4 channels instead of 6
void IR_NRF_Timer_Channel_Deinit(NRF_TIMER_Type* timer, IR_NRF_Timer_ChannelTypeDef channel)
{
  timer->INTENCLR = 1 << (16 + channel);
  timer->CC[channel] = 0;
}

//note that TIMER0, TIMER1 and TIMER2 have 4 channels instead of 6
uint32_t IR_NRF_Timer_Get_Value(NRF_TIMER_Type* timer, IR_NRF_Timer_ChannelTypeDef channel)
{
	timer->TASKS_CAPTURE[channel] = 1;
  return (timer->CC[channel]);
}

void IR_NRF_Timer_Start(NRF_TIMER_Type* timer)
{
  timer->TASKS_START = 1;
}

void IR_NRF_Timer_Stop(NRF_TIMER_Type* timer)
{
  timer->TASKS_STOP = 1;
  timer->TASKS_CLEAR = 1;
}

void IR_NRF_Timer_Reset(NRF_TIMER_Type* timer)
{
	timer->TASKS_CLEAR = 1;
}
__WEAK void IR_Timer0_Callback(IR_NRF_Timer_ChannelTypeDef channel)
{
  IR_UNUSED_VAR(channel);
}

__WEAK void IR_Timer1_Callback(IR_NRF_Timer_ChannelTypeDef channel)
{
  IR_UNUSED_VAR(channel);
}

__WEAK void IR_Timer2_Callback(IR_NRF_Timer_ChannelTypeDef channel)
{
  IR_UNUSED_VAR(channel);
}

__WEAK void IR_Timer3_Callback(IR_NRF_Timer_ChannelTypeDef channel)
{
  IR_UNUSED_VAR(channel);
}

__WEAK void IR_Timer4_Callback(IR_NRF_Timer_ChannelTypeDef channel)
{
  IR_UNUSED_VAR(channel);
}
