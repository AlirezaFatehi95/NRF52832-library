#include "IR_NRF52832_Timer.h"

/*
TimerFreq = 16M (always it is)
PS = Prescaler0 ~ Prescaler9 (0 ~ 9)
Prescaler = 2^PS
time = (1 / (TimerFreq / Prescaler) * TIMER_COUNTER

e.g.:
PS = 9 >> Prescaler = 512
TIMER_COUNTER = 20000
timer = 640ms
*/

#define TIMER_0_CHANNEL_2_COUNTER    20000
#define TIMER_0_CHANNEL_2_PRIORITY   7

//redefining the timer 0 callback (this function is defined as weak)
void IR_Timer0_Callback(IR_NRF_Timer_ChannelTypeDef channel)
{
  if(channel == IR_NRF_TIMER_CHANNEL_2)
  {
    //do someting every 640ms after the IR_NRF_Timer_Start function call
  }
}

int main()
{
  //priority must be set between 0(highest) to 7(lowest)
  IR_NRF_Timer_Init(NRF_TIMER0, IR_NRF_TIMER_TIMERMODE, IR_NRF_TIMER_BITMODE_32, IR_NRF_TIMER_PRESCALER_9, true, TIMER_0_CHANNEL_2_PRIORITY);
  //TIMER0 & TIMER1 & TIMER2 are having 3 channels instead of 5
  //channel event must also be enabled
  IR_NRF_Timer_Set_Channel(NRF_TIMER0, IR_NRF_TIMER_CHANNEL_2, TIMER_0_CHANNEL_2_COUNTER, true);
  IR_NRF_Timer_Start(NRF_TIMER0);
  while(1)
  {
    //do nothing!!!
  }
}
