#ifndef __IR_NRF52832_POWER_H__
#define __IR_NRF52832_POWER_H__

#include "IR_NRF52832_Common.h"

#define __IR_NRF_Power_Enable_SystemOff()   NRF_POWER->SYSTEMOFF = 1
#define __IR_NRF_Power_Enable_DCDC()        NRF_POWER->DCDCEN = 1
#define __IR_NRF_Power_Disable_DCDC()       NRF_POWER->DCDCEN = 0
#define __IR_NRF_Power_Get_DCDC_Status()    NRF_POWER->DCDCEN

typedef enum
{
  IR_NRF_POWER_MODE_CONSTANTLATENCY = 0,
  IR_NRF_POWER_MODE_LOWPOWER        = 1
}IR_NRF_Power_ModeTypeDef;

typedef enum
{
  IR_NRF_POWER_IRQ_NONE       = 0,
  IR_NRF_POWER_IRQ_POFWARN    = 4,
  IR_NRF_POWER_IRQ_SLEEPENTER = 32,
  IR_NRF_POWER_IRQ_SLEEPEXIT  = 64
}IR_NRF_Power_IRQTypeDef;

typedef enum
{
  IR_NRF_POWER_NO_POFCON       = 0,
  IR_NRF_POWER_THRESHOLD_1_V_7 = 4,
  IR_NRF_POWER_THRESHOLD_1_V_8 = 5,
  IR_NRF_POWER_THRESHOLD_1_V_9 = 6,
  IR_NRF_POWER_THRESHOLD_2_V_0 = 7,
  IR_NRF_POWER_THRESHOLD_2_V_1 = 8,
  IR_NRF_POWER_THRESHOLD_2_V_2 = 9,
  IR_NRF_POWER_THRESHOLD_2_V_3 = 10,
  IR_NRF_POWER_THRESHOLD_2_V_4 = 11,
  IR_NRF_POWER_THRESHOLD_2_V_5 = 12,
  IR_NRF_POWER_THRESHOLD_2_V_6 = 13,
  IR_NRF_POWER_THRESHOLD_2_V_7 = 14,
  IR_NRF_POWER_THRESHOLD_2_V_8 = 15,
}IR_NRF_Power_POFCON_TresholdTypeDef;

typedef enum
{
  IR_NRF_POWER_PWRDEFECT = 0,
  IR_NRF_POWER_RESETPIN  = 1,
  IR_NRF_POWER_DOG       = 2,
  IR_NRF_POWER_SREQ      = 3,
  IR_NRF_POWER_LOCKUP    = 4,
  IR_NRF_POWER_OFF       = 5,
  IR_NRF_POWER_LPCOMP    = 6,
  IR_NRF_POWER_DIF       = 7,
  IR_NRF_POWER_NFC       = 8
}IR_NRF_Power_ResetTypeDef;

typedef enum
{
  IR_NRF_POWER_RAM_BASE0 = 0,
  IR_NRF_POWER_RAM_BASE1 = 1,
  IR_NRF_POWER_RAM_BASE2 = 2,
  IR_NRF_POWER_RAM_BASE3 = 3,
  IR_NRF_POWER_RAM_BASE4 = 4,
  IR_NRF_POWER_RAM_BASE5 = 5,
  IR_NRF_POWER_RAM_BASE6 = 6,
  IR_NRF_POWER_RAM_BASE7 = 7
}IR_NRF_Power_RAM_BaseTypeDef;

typedef enum
{
  IR_NRF_Power_RAM_SECTION0 = 0,
  IR_NRF_Power_RAM_SECTION1 = 1
}IR_NRF_Power_RAM_SectionTypeDef;


void IR_NRF_Power_Init(IR_NRF_Power_ModeTypeDef power_mode, bool dcdc_en,
                       IR_NRF_Power_POFCON_TresholdTypeDef threshold);
void IR_NRF_Power_Clear_IRQ(IR_NRF_Power_IRQTypeDef irq);
void IR_NRF_Power_Set_IRQ(IR_NRF_Power_IRQTypeDef irq);
IR_NRF_Power_ResetTypeDef IR_NRF_POWER_Get_Reset_Flag(void);
void IR_NRF_Power_Set_POFCON(IR_NRF_Power_POFCON_TresholdTypeDef threshold);
void IR_NRF_Power_Set_RAM_Power(bool pwr_en, bool retention_en, IR_NRF_Power_RAM_BaseTypeDef base,
                                IR_NRF_Power_RAM_SectionTypeDef section);
void IR_Power_Callback(IR_NRF_Power_IRQTypeDef irq);

#endif /*__IR_NRF52832_POWER_H__*/
