#include "IR_NRF52832_Power.h"

//TODO: GPREGRET1 and GPREGRET2 functions must be written

void IR_NRF_Power_Init(IR_NRF_Power_ModeTypeDef power_mode, bool dcdc_en,
                       IR_NRF_Power_POFCON_TresholdTypeDef threshold)
{
  if(dcdc_en)
    __IR_NRF_Power_Enable_DCDC();
  else
    __IR_NRF_Power_Disable_DCDC();
  
  if(power_mode == IR_NRF_POWER_MODE_CONSTANTLATENCY)
    NRF_POWER->TASKS_CONSTLAT = 1;
  else
    NRF_POWER->TASKS_LOWPWR = 1;
  
  //turning on all ram section
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE0, IR_NRF_Power_RAM_SECTION0);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE0, IR_NRF_Power_RAM_SECTION1);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE1, IR_NRF_Power_RAM_SECTION0);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE1, IR_NRF_Power_RAM_SECTION1);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE2, IR_NRF_Power_RAM_SECTION0);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE2, IR_NRF_Power_RAM_SECTION1);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE3, IR_NRF_Power_RAM_SECTION0);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE3, IR_NRF_Power_RAM_SECTION1);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE4, IR_NRF_Power_RAM_SECTION0);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE4, IR_NRF_Power_RAM_SECTION1);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE5, IR_NRF_Power_RAM_SECTION0);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE5, IR_NRF_Power_RAM_SECTION1);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE6, IR_NRF_Power_RAM_SECTION0);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE6, IR_NRF_Power_RAM_SECTION1);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE7, IR_NRF_Power_RAM_SECTION0);
  IR_NRF_Power_Set_RAM_Power(true, false, IR_NRF_POWER_RAM_BASE7, IR_NRF_Power_RAM_SECTION1);

  IR_NRF_Power_Set_POFCON(threshold);
}

void IR_NRF_Power_Clear_IRQ(IR_NRF_Power_IRQTypeDef irq)
{
  NRF_POWER->INTENCLR = irq;
}

void IR_NRF_Power_Set_IRQ(IR_NRF_Power_IRQTypeDef irq)
{
  NRF_POWER->INTENSET = irq;
}

//this function holds the reason of last chip reset
IR_NRF_Power_ResetTypeDef IR_NRF_POWER_Get_Reset_Flag(void)
{
  if(NRF_POWER->RESETREAS & 0x00000001)
    return IR_NRF_POWER_RESETPIN;
  else if(NRF_POWER->RESETREAS & 0x00000002)
    return IR_NRF_POWER_DOG;
  else if(NRF_POWER->RESETREAS & 0x00000004)
    return IR_NRF_POWER_SREQ;
  else if(NRF_POWER->RESETREAS & 0x00000008)
    return IR_NRF_POWER_LOCKUP;
  else if(NRF_POWER->RESETREAS & 0x00010000)
    return IR_NRF_POWER_OFF;
  else if(NRF_POWER->RESETREAS & 0x00020000)
    return IR_NRF_POWER_LPCOMP;
  else if(NRF_POWER->RESETREAS & 0x00040000)
    return IR_NRF_POWER_DIF;
  else if(NRF_POWER->RESETREAS & 0x00080000)
    return IR_NRF_POWER_NFC;
  else
    return IR_NRF_POWER_PWRDEFECT;
}

void IR_NRF_Power_Set_POFCON(IR_NRF_Power_POFCON_TresholdTypeDef threshold)
{
  if(threshold == IR_NRF_POWER_NO_POFCON)
    NRF_POWER->POFCON = 0;
  else
    NRF_POWER->POFCON = (threshold << 1) | 1;
}

void IR_NRF_Power_Set_RAM_Power(bool pwr_en, bool retention_en, IR_NRF_Power_RAM_BaseTypeDef base,
                                IR_NRF_Power_RAM_SectionTypeDef section)
{
  if(pwr_en)
    NRF_POWER->RAM[base].POWERSET = 1 << section;
  else
    NRF_POWER->RAM[base].POWERCLR = 1 << section;
  
  if(retention_en)
    NRF_POWER->RAM[base].POWERSET = 1 << (section + 16);
  else
    NRF_POWER->RAM[base].POWERCLR = 1 << (section + 16);  
}

__WEAK void IR_Power_Callback(IR_NRF_Power_IRQTypeDef irq)
{
  IR_UNUSED_VAR(irq);
}
