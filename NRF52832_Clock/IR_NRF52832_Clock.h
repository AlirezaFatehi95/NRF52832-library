#ifndef __IR_NRF52832_CLOCK_H__
#define __IR_NRF52832_CLOCK_H__

#include "IR_NRF52832_Common.h"

#define CTIV_MAX 127
#define CTIV_MIN 1

#define __IR_NRF_CLOCK_LF_CALIBRATION(ct) do{\
                                              CONSTRAIN(ct, CTIV_MAX, CTIV_MIN);\
                                              NRF_CLOCK->CTIV = ct;\
                                              NRF_CLOCK->TASKS_CAL = 1;\
                                              while(!NRF_CLOCK->EVENTS_DONE);\
                                            }while(0)
                                            
typedef enum
{
  IR_NRF_CLOCK_IRQ_NONE         = 0,
  IR_NRF_CLOCK_IRQ_HFCLKSTARTED = 1,
  IR_NRF_CLOCK_IRQ_LFCLKSTARTED = 2,
  IR_NRF_CLOCK_IRQ_DONE         = 8,
  IR_NRF_CLOCK_IRQ_CTTO         = 16
}IR_NRF_Clock_IRQTypeDef;

//note: the trace pin clock is devied by 2 (eg: IR_NRF_CLOCK_TRACEPORTSPEED_32MHZ => trace pin clock is 16MHz)
typedef enum
{
  IR_NRF_CLOCK_TRACEPORTSPEED_32MHZ = 0,
  IR_NRF_CLOCK_TRACEPORTSPEED_16MHZ = 1,
  IR_NRF_CLOCK_TRACEPORTSPEED_8MHZ  = 2,
  IR_NRF_CLOCK_TRACEPORTSPEED_4MHZ  = 3,
}IR_NRF_Clock_TracePortSpeedTypeDef;

typedef enum
{
  IR_NRF_CLOCK_TRACEMUX_GPIO     = 0,
  IR_NRF_CLOCK_TRACEMUX_SERIAL   = 1,
  IR_NRF_CLOCK_TRACEMUX_PARALLEL = 2
}IR_NRF_Clock_TraceMUXTypeDef;

typedef enum
{
  IR_NRF_CLOCK_HFCLKSOURCE_INTERNAL = 0,
  IR_NRF_CLOCK_HFCLKSOURCE_EXTERNAL = 1
}IR_NRF_Clock_HFCLKSourceTypeDef;

typedef enum
{
  IR_NRF_CLOCK_HFCLKSTATE_NOTRUNNING = 0,
  IR_NRF_CLOCK_HFCLKSTATE_RUNNING    = 1
}IR_NRF_Clock_HFCLKStateTypeDef;

typedef enum
{
  IR_NRF_CLOCK_LFCLKSOURCE_INTERNAL    = 0,
  IR_NRF_CLOCK_LFCLKSOURCE_EXTERNAL    = 1,
  IR_NRF_CLOCK_LFCLKSOURCE_SYNTHESIZED = 2
}IR_NRF_Clock_LFCLKSourceTypeDef;

typedef enum
{
  IR_NRF_CLOCK_LFCRYSTAL_NORMAL    = 0,
  IR_NRF_CLOCK_LFCRYSTAL_LOWSWING  = 2,
  IR_NRF_CLOCK_LFCRYSTAL_FULLSWING = 3
}IR_NRF_Clock_LFCrystalTypeDef;

typedef enum
{
  IR_NRF_CLOCK_LFCLKSTATE_NOTRUNNING = 0,
  IR_NRF_CLOCK_LFCLKSTATE_RUNNING    = 1
}IR_NRF_Clock_LFCLKStateTypeDef;

void IR_NRF_Clock_Init(bool ext_hf_clk_en,
                       IR_NRF_Clock_LFCLKSourceTypeDef lf_clk_src,
                       IR_NRF_Clock_LFCrystalTypeDef ext_lf_crystal,
                       IR_NRF_Clock_TracePortSpeedTypeDef trace_port_speed,
                       IR_NRF_Clock_TraceMUXTypeDef trace_mux);
void IR_NRF_Clock_Set_IRQ(IR_NRF_Clock_IRQTypeDef irq);
void IR_NRF_Clock_Clear_IRQ(IR_NRF_Clock_IRQTypeDef irq);
void IR_NRF_Clock_LFCLK_Set_Source(IR_NRF_Clock_LFCLKSourceTypeDef src,
                                   IR_NRF_Clock_LFCrystalTypeDef crystal);
void IR_NRF_Clock_LFCLK_Start(void);
void IR_NRF_Clock_Set_CalibrationTimerInterval(uint8_t second);
void IR_NRF_Clock_Set_TracePortSpeed(IR_NRF_Clock_TracePortSpeedTypeDef speed);
void IR_NRF_Clock_Set_TraceMUX(IR_NRF_Clock_TraceMUXTypeDef mux);
IR_NRF_Clock_HFCLKSourceTypeDef IR_NRF_Clock_Get_HFCLK_Source(void);
IR_NRF_Clock_HFCLKStateTypeDef IR_NRF_Clock_Get_HFCLK_State(void);
IR_NRF_Clock_LFCLKSourceTypeDef IR_NRF_Clock_Get_LFCLK_Source(void);
IR_NRF_Clock_LFCLKStateTypeDef IR_NRF_Clock_Get_LFCLK_State(void);

#endif /*__IR_NRF52832_CLOCK_H__*/
