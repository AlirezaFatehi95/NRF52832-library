#include "IR_NRF52832_Interrupt.h"

volatile uint32_t DummyVar = 0;

void POWER_CLOCK_IRQHandler(void)
{
  if(NRF_POWER->EVENTS_POFWARN)
  {
    NRF_POWER->EVENTS_POFWARN = 0;
    IR_Power_Callback(IR_NRF_POWER_IRQ_POFWARN);
  }
  if(NRF_POWER->EVENTS_SLEEPENTER)
  {
    NRF_POWER->EVENTS_SLEEPENTER = 0;
    IR_Power_Callback(IR_NRF_POWER_IRQ_SLEEPENTER);
  }
  if(NRF_POWER->EVENTS_SLEEPEXIT)
  {
    NRF_POWER->EVENTS_SLEEPEXIT = 0;
    IR_Power_Callback(IR_NRF_POWER_IRQ_SLEEPEXIT);
  }
}

void TIMER0_IRQHandler(void)
{
  if(NRF_TIMER0->EVENTS_COMPARE[0])
  {
    NRF_TIMER0->EVENTS_COMPARE[0] = 0;
    IR_Timer0_Callback(IR_NRF_TIMER_CHANNEL_0);
  }
  if(NRF_TIMER0->EVENTS_COMPARE[1])
  {
    NRF_TIMER0->EVENTS_COMPARE[1] = 0;
    IR_Timer0_Callback(IR_NRF_TIMER_CHANNEL_1);
  }
  if(NRF_TIMER0->EVENTS_COMPARE[2])
  {
    NRF_TIMER0->EVENTS_COMPARE[2] = 0;
    IR_Timer0_Callback(IR_NRF_TIMER_CHANNEL_2);
  }
	if(NRF_TIMER0->EVENTS_COMPARE[3])
  {
    NRF_TIMER0->EVENTS_COMPARE[3] = 0;
    IR_Timer0_Callback(IR_NRF_TIMER_CHANNEL_3);
  }
  DummyVar = NRF_TIMER0->PRESCALER;//just to buy time for the "NRF_TIMER0->EVENTS_COMPARE[x] = 0;" to take effect
}

void TIMER1_IRQHandler(void)
{
  if(NRF_TIMER1->EVENTS_COMPARE[0])
  {
    NRF_TIMER1->EVENTS_COMPARE[0] = 0;
    IR_Timer1_Callback(IR_NRF_TIMER_CHANNEL_0);
  }
  if(NRF_TIMER1->EVENTS_COMPARE[1])
  {
    NRF_TIMER1->EVENTS_COMPARE[1] = 0;
    IR_Timer1_Callback(IR_NRF_TIMER_CHANNEL_1);
  }
  if(NRF_TIMER1->EVENTS_COMPARE[2])
  {
    NRF_TIMER1->EVENTS_COMPARE[2] = 0;
    IR_Timer1_Callback(IR_NRF_TIMER_CHANNEL_2);
  }
	if(NRF_TIMER1->EVENTS_COMPARE[3])
  {
    NRF_TIMER1->EVENTS_COMPARE[3] = 0;
    IR_Timer1_Callback(IR_NRF_TIMER_CHANNEL_3);
  }
  DummyVar = NRF_TIMER1->PRESCALER;//just to buy time for the "NRF_TIMER1->EVENTS_COMPARE[x] = 0;" to take effect
}

void TIMER2_IRQHandler(void)
{
  if(NRF_TIMER2->EVENTS_COMPARE[0])
  {
    NRF_TIMER2->EVENTS_COMPARE[0] = 0;
    IR_Timer2_Callback(IR_NRF_TIMER_CHANNEL_0);
  }
  if(NRF_TIMER2->EVENTS_COMPARE[1])
  {
    NRF_TIMER2->EVENTS_COMPARE[1] = 0;
    IR_Timer2_Callback(IR_NRF_TIMER_CHANNEL_1);
  }
  if(NRF_TIMER2->EVENTS_COMPARE[2])
  {
    NRF_TIMER2->EVENTS_COMPARE[2] = 0;
    IR_Timer2_Callback(IR_NRF_TIMER_CHANNEL_2);
  }
	if(NRF_TIMER2->EVENTS_COMPARE[3])
  {
    NRF_TIMER2->EVENTS_COMPARE[3] = 0;
    IR_Timer2_Callback(IR_NRF_TIMER_CHANNEL_3);
  }
  DummyVar = NRF_TIMER2->PRESCALER;//just to buy time for the "NRF_TIMER2->EVENTS_COMPARE[x] = 0;" to take effect
}

void TIMER3_IRQHandler(void)
{
  if(NRF_TIMER3->EVENTS_COMPARE[0])
  {
    NRF_TIMER3->EVENTS_COMPARE[0] = 0;
    IR_Timer3_Callback(IR_NRF_TIMER_CHANNEL_0);
  }
  if(NRF_TIMER3->EVENTS_COMPARE[1])
  {
    NRF_TIMER3->EVENTS_COMPARE[1] = 0;
    IR_Timer3_Callback(IR_NRF_TIMER_CHANNEL_1);
  }
  if(NRF_TIMER3->EVENTS_COMPARE[2])
  {
    NRF_TIMER3->EVENTS_COMPARE[2] = 0;
    IR_Timer3_Callback(IR_NRF_TIMER_CHANNEL_2);
  }
	if(NRF_TIMER3->EVENTS_COMPARE[3])
  {
    NRF_TIMER3->EVENTS_COMPARE[3] = 0;
    IR_Timer3_Callback(IR_NRF_TIMER_CHANNEL_3);
  }
	if(NRF_TIMER3->EVENTS_COMPARE[4])
  {
    NRF_TIMER3->EVENTS_COMPARE[4] = 0;
    IR_Timer3_Callback(IR_NRF_TIMER_CHANNEL_4);
  }
	if(NRF_TIMER3->EVENTS_COMPARE[5])
  {
    NRF_TIMER3->EVENTS_COMPARE[5] = 0;
    IR_Timer3_Callback(IR_NRF_TIMER_CHANNEL_5);
  }
  DummyVar = NRF_TIMER3->PRESCALER;//just to buy time for the "NRF_TIMER3->EVENTS_COMPARE[x] = 0;" to take effect
}

void TIMER4_IRQHandler(void)
{
  if(NRF_TIMER4->EVENTS_COMPARE[0])
  {
    NRF_TIMER4->EVENTS_COMPARE[0] = 0;
    IR_Timer4_Callback(IR_NRF_TIMER_CHANNEL_0);
  }
  if(NRF_TIMER4->EVENTS_COMPARE[1])
  {
    NRF_TIMER4->EVENTS_COMPARE[1] = 0;
    IR_Timer4_Callback(IR_NRF_TIMER_CHANNEL_1);
  }
  if(NRF_TIMER4->EVENTS_COMPARE[2])
  {
    NRF_TIMER4->EVENTS_COMPARE[2] = 0;
    IR_Timer4_Callback(IR_NRF_TIMER_CHANNEL_2);
  }
	if(NRF_TIMER4->EVENTS_COMPARE[3])
  {
    NRF_TIMER4->EVENTS_COMPARE[3] = 0;
    IR_Timer4_Callback(IR_NRF_TIMER_CHANNEL_3);
  }
	if(NRF_TIMER4->EVENTS_COMPARE[4])
  {
    NRF_TIMER4->EVENTS_COMPARE[4] = 0;
    IR_Timer4_Callback(IR_NRF_TIMER_CHANNEL_4);
  }
	if(NRF_TIMER4->EVENTS_COMPARE[5])
  {
    NRF_TIMER4->EVENTS_COMPARE[5] = 0;
    IR_Timer4_Callback(IR_NRF_TIMER_CHANNEL_5);
  }
  DummyVar = NRF_TIMER4->PRESCALER;//just to buy time for the "NRF_TIMER4->EVENTS_COMPARE[x] = 0;" to take effect
}

void GPIOTE_IRQHandler(void)
{
  // if(NRF_GPIOTE->EVENTS_PORT == 1)
  // {
  //   NRF_P0->LATCH
  // }
  for(uint8_t i = 0; i < 8; i++)
  {
    if(NRF_GPIOTE->EVENTS_IN[i])
    {
      NRF_GPIOTE->EVENTS_IN[i] = 0;
      IR_GPIOE_Callback(i);
    }
  }
}
