#ifndef __IR_NRF52832_INTERRUPT_H__
#define __IR_NRF52832_INTERRUPT_H__

#include "IR_NRF52832_Power.h"
#include "IR_NRF52832_Timer.h"
#include "IR_NRF52832_GPIO.h"

#endif /*__IR_NRF52832_INTERRUPT_H__*/
