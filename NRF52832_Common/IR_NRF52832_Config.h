#ifndef __IR_NRF52832_CONFIG_H__
#define __IR_NRF52832_CONFIG_H__

#include "IR_NRF52832_Clock.h"
#include "IR_NRF52832_Common.h"
#include "IR_NRF52832_GPIO.h"
#include "IR_NRF52832_Interrupt.h"
#include "IR_NRF52832_Power.h"
#include "IR_NRF52832_Timer.h"

#endif
